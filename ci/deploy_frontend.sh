#!/bin/bash

ssh -p22 $DEPLOY_ENVIRONMENT "rm -r $DEPLOY_FOLDER/$1/* || true"
scp -rp -P22 ./dist/* $DEPLOY_ENVIRONMENT:$DEPLOY_FOLDER/$1
