#!/bin/bash
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
eval "$(ssh-agent -s)"
ssh-add <(echo "$DEPLOY_PRIVATE_KEY")
mkdir -p ~/.ssh
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
