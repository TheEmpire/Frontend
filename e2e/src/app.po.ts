import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/en');
  }

  getLoginLabel() {
    return element(by.cssContainingText('.text-uppercase', 'Please enter your imperial id')).getText();
  }
}
