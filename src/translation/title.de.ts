export enum Title {
  auth = 'Imperiales Netzwerk',
  legalNotice = 'Impressum',
  dataProtection = 'Datenschutzerkärung',
  copyright = 'Urheberrecht',
  reset = 'Passwort zurücksetzen',
  validate = 'Mailverifizierung'
}
