export const serverMsg = {
  mailValidated: 'Email wurde erfolgreich validiert, Weiterleitung in 5 Sekunden',
  IDHashMissing: 'Imperiale ID oder Hash fehlen, überprüfe den Link',
  ValidationFailed: 'Email Validierung ist fehlgeschlagen, versuch es erneut',
  MailMissing: 'Email ist nicht eingegeben worden',
  ErrorMailUpdate: 'Fehler beim Ändern der Email in der Datenbank',
  MailSend: 'Email wurde erfolgreich gesendet',
  ErrorMailSend: 'Beim Email Senden ist ein Fehler aufgetreten',
  FormFilledIncorrect: 'Das Formular wurde nicht korrekt ausgefüllt',
  UserCreated: 'Dein User wurde erfolgreich angelegt',
  DatabaseError: 'Die Datenbankoperation konnte nicht ausgeführt werden',
  UserExists: 'Der User existiert bereits, logge dich ein',
  IDPwdMissing: 'Imperiale ID oder Passwort nicht eingegeben',
  LoginSuccess: 'Login erfolgreich',
  AccessDenied: 'Zugang verweigert, überprüfe deine imperiale ID und Passwort',
  PwdUpdated: 'Das Passwort wurde erfolgreich geupdatet, Weiterleitung in 5 Sekunden',
  IDLinkWrong: 'User existiert nicht oder Link ist falsch',
  UserNotFound: 'User existiert nicht'
};
