export enum Error {
  idTooShort = 'ID ist zu kurz',
  pwdTooShort = 'Passwort ist zu kurz',
  realNameTooShort = 'Bitte gib Vor- und Nachnamen oder gar keinen an',
  emailInvalid = 'Bitte gib eine gültige Email-Addresse an',
  dobInvalid = 'Bitte gib dein Geburtsdatum oder dein Geburtsjahr an',
  pwdInvalid = 'Bitte gib ein Passwort an, welches den gegebenen Kriterien entspricht',
  loginInvalid = 'Imperiale ID oder Passwort nicht eingegeben',
  registrationInvalid = 'Formular wurde nicht korrekt ausgefüllt',
  serverNotReachable = 'Server nicht erreichbar, versuche es erneut',
  validationParamsMissing = 'Der Link ist nicht korrekt, versuch diesen erneut zu kopieren. Wenn das Problem weiterbesteht, ' +
    'kontaktiere Support'
}
