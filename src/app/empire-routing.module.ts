import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ValidateEmailComponent} from './validate-email/validate-email.component';
import {AuthenticationComponent} from './authentication/authentication.component';
import {RouteGuard} from './route.guard';
import {RouteVerifiedGuard} from './route-verified.guard';
import {DisclosureComponent} from './legal/disclosure/disclosure.component';
import {DataSecurityComponent} from './legal/data-security/data-security.component';
import {CopyrightComponent} from './legal/copyright/copyright.component';
import {ValidationEmailComponent} from './validation-email/validation-email.component';
import {ResetComponent} from './reset/reset.component';
import {ResetLinkComponent} from './reset-link/reset-link.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [RouteVerifiedGuard] },
  { path: 'login', component: AuthenticationComponent },
  { path: 'validateEmail', component: ValidateEmailComponent, canActivate: [RouteGuard] },
  { path: 'legal', component: DisclosureComponent },
  { path: 'dataSecurity', component: DataSecurityComponent },
  { path: 'copyright', component: CopyrightComponent },
  { path: 'validation', component: ValidationEmailComponent },
  { path: 'reset', component: ResetComponent },
  { path: 'resetPwd', component: ResetLinkComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class EmpireRoutingModule {}
