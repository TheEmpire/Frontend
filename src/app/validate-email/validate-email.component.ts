import {Component, OnInit} from '@angular/core';
import {NgbTooltip} from '@ng-bootstrap/ng-bootstrap';
import {NgModel} from '@angular/forms';
import {Regex} from '../model/regex';
import {Error} from '../model/enums/error';
import {RestConnectionService} from '../service/rest-connection.service';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';
import {LanguageService} from '../service/language.service';
import {Title} from '../model/enums/title';

@Component({
  selector: 'empire-validate-email',
  templateUrl: './validate-email.component.html',
  styleUrls: ['./validate-email.component.scss'],
})
export class ValidateEmailComponent implements OnInit {
  mail: string;
  id: string;
  newMail: string;
  Regex = Regex;
  Errors = Error;
  newMsg: boolean;
  error: boolean;
  alertMessage: string;
  sending: boolean;

  constructor(private restConn: RestConnectionService, private router: Router,
              private authService: AuthService, private language: LanguageService) {
    this.language.setTitle(Title.validate);
  }

  ngOnInit() {
    this.mail = this.authService.getEmail();
    this.id = this.authService.getID();
    this.sendMail();
  }

  processChange(errorTip: NgbTooltip, value: NgModel): void {
    if (value.valid) {
      errorTip.close();
    } else {
      errorTip.open();
    }
    // TODO Typerror msg undefined bei token fehler
  }

  sendMail() {
    this.sending = true;
    this.restConn.validateEmail(this.mail, this.id).subscribe(value => {
      this.error = value.msg !== 'MailSend';
      this.newMsg = true;
      this.alertMessage = value.msg;
      this.sending = false;
    }, error => {
      this.newMsg = true;
      this.error = true;
      this.alertMessage = error;
      this.sending = false;
    });
  }

  updateMail() {
    this.sending = true;
    this.restConn.updateEmail(this.id, this.newMail).subscribe(value => {
      if (value.msg !== 'MailSend') {
        this.error = true;
      } else {
        this.error = false;
        this.mail = this.newMail;
        this.newMail = undefined;
      }
      this.newMsg = true;
      this.alertMessage = value.msg;
      this.sending = false;
    }, error => {
      this.newMsg = true;
      this.error = true;
      this.alertMessage = error;
      this.sending = false;
    });
  }
}
