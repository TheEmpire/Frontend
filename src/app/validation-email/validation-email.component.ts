import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestConnectionService} from '../service/rest-connection.service';
import {Error} from '../model/enums/error';
import {AuthService} from '../service/auth.service';
import {LanguageService} from '../service/language.service';
import {Title} from '../model/enums/title';
@Component({
  selector: 'empire-validation-email',
  templateUrl: './validation-email.component.html',
  styleUrls: ['./validation-email.component.scss']
})
export class ValidationEmailComponent implements OnInit {
  hash: string;
  id: string;
  newMsg: boolean;
  error: boolean;
  alertMessage: string;
  sending: boolean;

  constructor(private route: ActivatedRoute, private rest: RestConnectionService,
              private authService: AuthService, private router: Router, private language: LanguageService) {
    this.language.setTitle(Title.validate);
  }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe(params => {
        if (params.has('hash') && params.has('id')) {
          this.hash = params.get('hash');
          this.id = params.get('id');
        } else {
          this.alertMessage = Error.validationParamsMissing;
          this.error = true;
          this.newMsg = true;
        }
      });
  }

  public validateEmail(): void {
    this.sending = true;
    this.rest.validationEmail(this.hash, this.id).subscribe(msg => {
      this.sending = false;
      this.error = msg.msg !== 'MailValidated';
      this.newMsg = true;
      this.alertMessage = msg.msg;
      if (msg.msg === 'MailValidated') {
        this.authService.deleteToken();
        setTimeout(() => this.router.navigate(['/login']), 5000);
      }
    }, error => {
      this.sending = false;
      this.error = true;
      this.newMsg = true;
      this.alertMessage = error;
    });
  }
}
