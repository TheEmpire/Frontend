import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AppComponent} from './app.component';
import {LoginComponent} from './authentication/login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {EmpireRoutingModule} from './empire-routing.module';
import {ValidateEmailComponent} from './validate-email/validate-email.component';
import {AuthenticationComponent} from './authentication/authentication.component';
import {RegistrationComponent} from './authentication/registration/registration.component';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faGlobe} from '@fortawesome/free-solid-svg-icons/faGlobe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FocusedDirective} from './model/focused.directive';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {JwtModule} from '@auth0/angular-jwt';
import {DataSecurityComponent} from './legal/data-security/data-security.component';
import {DisclosureComponent} from './legal/disclosure/disclosure.component';
import {FooterComponent} from './legal/footer/footer.component';
import {ContactFormComponent} from './legal/contact-form/contact-form.component';
import {LanguageSwitchComponent} from './legal/language-switch/language-switch.component';
import { CopyrightComponent } from './legal/copyright/copyright.component';
import {faSignInAlt} from '@fortawesome/free-solid-svg-icons/faSignInAlt';
import {TimezonePickerComponent} from './timezonePicker/timezone-picker.component';
import {TimezonePickerPipe} from './timezonePicker/timezone-picker.pipe';
import { ValidationEmailComponent } from './validation-email/validation-email.component';
import { LogoutComponent } from './authentication/logout/logout.component';
import { TranslateServerPipe } from './model/translate-server.pipe';
import { ResetComponent } from './reset/reset.component';
import { ResetLinkComponent } from './reset-link/reset-link.component';

library.add(faSignInAlt);
library.add(faGlobe);
export function tokenGetter() {
  return sessionStorage.getItem('EmpireToken');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ValidateEmailComponent,
    AuthenticationComponent,
    RegistrationComponent,
    FocusedDirective,
    DataSecurityComponent,
    DisclosureComponent,
    FooterComponent,
    ContactFormComponent,
    LanguageSwitchComponent,
    CopyrightComponent,
    TimezonePickerComponent,
    TimezonePickerPipe,
    ValidationEmailComponent,
    LogoutComponent,
    TranslateServerPipe,
    ResetComponent,
    ResetLinkComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    EmpireRoutingModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularSvgIconModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['rest.cloud-in.space/empire'],
        blacklistedRoutes: ['rest.cloud-in.space/empire/authentication']
      }
    })
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
