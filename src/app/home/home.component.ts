import {Component, OnInit} from '@angular/core';
import {RestConnectionService} from '../service/rest-connection.service';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'empire-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private restConn: RestConnectionService, private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

}
