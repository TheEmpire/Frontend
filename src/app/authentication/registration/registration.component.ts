import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Regex} from '../../model/regex';
import {RestConnectionService} from '../../service/rest-connection.service';
import {Router} from '@angular/router';
import {AuthData} from '../../model/AuthData';
import {NgbTooltip} from '@ng-bootstrap/ng-bootstrap';
import {NgModel} from '@angular/forms';
import {Error} from '../../model/enums/error';
import {TimezonePickerService} from '../../timezonePicker/timezone-picker.service';

@Component({
  selector: 'empire-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  @Input() authData: AuthData;
  @Output() msg: EventEmitter<string> = new EventEmitter<string>();
  Regex = Regex;
  Errors = Error;

  constructor(private restConn: RestConnectionService, private router: Router, protected service: TimezonePickerService) {
  }

  ngOnInit() {
  }

  processRegistration(): void {
    if (this.authData.isInvalidRegistration()) {
      this.emitMessage(Error.registrationInvalid);
    } else {
      this.emitMessage('loading');
      this.restConn.sendRegistration(this.authData).subscribe(
        response => {
          if (response.msg === 'UserCreated') {
            this.restConn.validateEmail(this.authData.email, this.authData.id);
            this.router.navigate(['/validateEmail']);
          } else {
            this.emitMessage(response.msg);
          }
        },
        error => this.emitMessage(error)
      );
    }
  }

  private emitMessage(msg: string): void {
    this.msg.emit(msg);
  }

  getValidityClass(input: string): string {
    if (input === undefined) {
      return 'invalid';
    } else {
      return 'valid';
    }
  }

  timezoneChange(input: string): void {
    // Timeout needed for AngularChangeDetection (on initial set)
    setTimeout(() => this.authData.timezone = input);
  }

  processChange(errorTip: NgbTooltip, value: NgModel): void {
    if (value.valid) {
      errorTip.close();
    } else {
      errorTip.open();
    }
  }
}
