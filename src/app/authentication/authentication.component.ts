import {Component, OnInit} from '@angular/core';
import {RestConnectionService} from '../service/rest-connection.service';
import {Error} from '../model/enums/error';
import {NgbTooltip} from '@ng-bootstrap/ng-bootstrap';
import {NgModel} from '@angular/forms';
import {AuthData} from '../model/AuthData';
import {AuthState} from '../model/AuthState';
import {LanguageService} from '../service/language.service';
import {Title} from '../model/enums/title';

@Component({
  selector: 'empire-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
  authData: AuthData;
  AuthState = AuthState;
  state = AuthState.name;
  Errors = Error;
  alertMessage: string;

  constructor(private restConn: RestConnectionService, private language: LanguageService) {
    this.authData = new AuthData();
    this.language.setTitle(Title.auth);
  }

  ngOnInit() {
  }

  performNext(): void {
    if (this.authData.id.length >= 5) {
      this.state = AuthState.nameLoading;
      this.restConn.userExists(this.authData.id).subscribe(value => {
        if (value.exists) {
          this.state = AuthState.login;
        } else {
          this.state = AuthState.register;
        }
      }, error => {
        this.state = AuthState.nameError;
        this.alertMessage = error;
      });
    }
  }

  processChange(errorTip: NgbTooltip, empireId: NgModel): void {
    if (empireId.valid) {
      errorTip.close();
    } else {
      errorTip.open();
    }
  }

  processMsg(msg: string, errorState: AuthState) {
    if (msg === 'changeID') {
      this.state = AuthState.name;
    } else if (msg === 'loading' && this.state > 0 && this.state < 10) {
      this.state += 10;
    } else {
      this.alertMessage = msg;
      this.state = errorState;
    }
  }

  isError(): boolean {
    return this.state < 0;
  }

  isLoading(): boolean {
    return this.state > 10;
  }

  invertError(): void {
    if (this.state < 0) {
      this.state = -this.state;
    }
  }
}
