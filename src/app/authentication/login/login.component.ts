import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RestConnectionService} from '../../service/rest-connection.service';
import {Router} from '@angular/router';
import {NgModel} from '@angular/forms';
import {Error} from '../../model/enums/error';
import {NgbTooltip} from '@ng-bootstrap/ng-bootstrap';
import {AuthData} from '../../model/AuthData';

@Component({
  selector: 'empire-login',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {
  @Output() msg = new EventEmitter<string>();
  errors = Error;
  @Input() authData: AuthData;
  constructor(private restConn: RestConnectionService, private router: Router) {
  }

  ngOnInit() { }

  performLogin(): void {
    if (this.authData.isInvalidLogin()) {
      this.msg.emit(Error.loginInvalid);
    } else {
      this.msg.emit('loading');
      this.restConn.performLogin(this.authData).subscribe(
        response => {
          if (response.msg === 'LoginSuccess') {
            this.router.navigate(['/home']);
          } else {
            this.msg.emit(response.msg);
          }
            },
          error => {
          this.msg.emit( error);
        }
      );
    }
  }

  processChange(errorTip: NgbTooltip, pwd: NgModel): void {
    if (pwd.valid) {
      errorTip.close();
    } else {
      errorTip.open();
    }
  }
}
