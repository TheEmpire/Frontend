import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../service/language.service';
import {Title} from '../../model/enums/title';

@Component({
  selector: 'empire-disclosure',
  templateUrl: './disclosure.component.html',
  styleUrls: ['../legal.scss']
})
export class DisclosureComponent implements OnInit {

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    this.language.setTitle(Title.legalNotice);
  }
}
