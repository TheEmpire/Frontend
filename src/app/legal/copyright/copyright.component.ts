import { Component, OnInit } from '@angular/core';
import {Title} from '../../model/enums/title';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'empire-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./../legal.scss', './copyright.component.scss']
})
export class CopyrightComponent implements OnInit {

  constructor(private language: LanguageService) { }

  ngOnInit() {
    this.language.setTitle(Title.copyright);
  }

  public getYear(): number {
    return new Date().getFullYear();
  }
}
