import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'empire-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['../legal.scss']
})
export class ContactFormComponent implements OnInit {
  message: string;
  contactMail: string;
  name: string;
  mailStatus = false;
  mailMsg: string;
  mailError: boolean;
  mailSending = false;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  sendMail(): void {
    this.mailSending = true;
    this.http.put<ServerMsg>('https://rest.cloud-in.space/sendMail', JSON.stringify(new JsonMsg(this.message, this.name, this.contactMail)),
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe((serverMsg: ServerMsg) => {
      this.mailStatus = true;
      this.mailMsg = serverMsg.msg;
      this.mailError = serverMsg.msg !== 'Mail was successfully send';
    }, (error: HttpErrorResponse) => {
      this.mailStatus = true;
      this.mailMsg = error.message;

      this.mailError = true;
    });
  }

  validateInput(): boolean {
    return this.message === null || this.message === undefined || this.message === '' ||
      this.name === null || this.name === undefined || this.name === '';
  }
}

class ServerMsg {
  msg: string;
}

class JsonMsg {
  msg: string;
  name: string;
  mail: string;

  constructor(msg: string, name: string, mail?: string) {
    this.msg = msg;
    this.name = name;
    this.mail = mail;
    if (this.mail === undefined || this.mail === null || this.mail === '') {
      this.mail = 'Not provided';
    }
  }
}
