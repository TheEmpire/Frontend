import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../service/language.service';
import {Title} from '../../model/enums/title';

@Component({
  selector: 'empire-data-security',
  templateUrl: './data-security.component.html',
  styleUrls: ['../legal.scss']
})
export class DataSecurityComponent implements OnInit {

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    this.language.setTitle(Title.dataProtection);
  }
}
