import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'empire-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  public getYear(): number {
    return new Date().getFullYear();
  }
}
