import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'empire-language-switch',
  templateUrl: './language-switch.component.html',
  styleUrls: ['./language-switch.component.scss']
})
export class LanguageSwitchComponent implements OnInit {

  constructor(public language: LanguageService) {
  }

  ngOnInit() {
  }
}
