import { Component, OnInit } from '@angular/core';
import {RestConnectionService} from '../service/rest-connection.service';
import {Title} from '../model/enums/title';
import {LanguageService} from '../service/language.service';
@Component({
  selector: 'empire-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  id: string;
  newMsg: boolean;
  error: boolean;
  alertMessage: string;
  sending: boolean;
  constructor(private rest: RestConnectionService, private language: LanguageService) {
    this.language.setTitle(Title.reset);
  }

  ngOnInit() {
  }

  public requestPwd(): void {
    this.sending = true;
    this.rest.requestPwd(this.id).subscribe(msg => {
      this.sending = false;
      this.error = msg.msg !== 'MailSend';
      this.newMsg = true;
      this.alertMessage = msg.msg;
    }, error => {
      this.sending = false;
      this.error = true;
      this.newMsg = true;
      this.alertMessage = error;
    });
  }
}
