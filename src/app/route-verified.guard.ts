import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteVerifiedGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    if (!this.auth.isVerified()) {
      this.router.navigate(['validateEmail']);
      return false;
    }
    return true;
  }
}
