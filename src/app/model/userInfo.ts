export class UserInfo {
  email: string;
  realName: string;
  dob: Date;
  doa: Date;
  residence: string;
  homeworld: string;
  balance: number;
}

export class UserInfoWithToken {
  userInfo: UserInfo;
  token: string;
}
