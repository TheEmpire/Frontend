export class ServerMsg {
  msg: string;
}

export class ServerMsgWithToken {
  msg: ServerMsg;
  token: string;
}
