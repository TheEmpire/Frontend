import {Regex} from './regex';

export class AuthData {
  id: string;
  pwd: string;
  realName?: string;
  email?: string;
  timezone?: string;
  dob?: string;

  isInvalidRegistration(): boolean {
    return this.timezone === '' ||
      this.timezone === null ||
      this.timezone === undefined ||
      this.email === '' ||
      this.id === '' ||
      this.pwd === '' ||
      this.dob === '' ||
      this.pwd.length < 8 ||
      this.pwd.match(Regex.pwd) === null ||
      this.email.match(Regex.email) === null ||
      this.dob.match(Regex.dob) === null ||
      (this.realName !== '' && this.realName !== undefined && this.realName.match(Regex.real) === null);
  }

  isInvalidLogin(): boolean {
    return this.id === '' || this.pwd === '' || this.pwd.length < 8;
  }
}
