export enum AuthState {
  name = 1,
  nameError = -1,
  nameLoading = 11,
  register = 2,
  registerError = -2,
  registerLoading = 12,
  login = 3,
  loginError = -3,
  loginLoading = 13
}
