import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[empireFocused]'
})
export class FocusedDirective implements OnInit {

  constructor(private hostElement: ElementRef) {
  }

  ngOnInit() {
    this.hostElement.nativeElement.focus();
  }
}
