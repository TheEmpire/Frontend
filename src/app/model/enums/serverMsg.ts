export const serverMsg = {
  mailValidated: 'Email was successfully validated, Redirecting in 5 seconds',
  IDHashMissing: 'Imperial ID or Hash are missing, check your link',
  ValidationFailed: 'Email Validation failed, try again',
  MailMissing: 'Email was not entered',
  ErrorMailUpdate: 'Error updating the Email in the database',
  MailSend: 'Email was successfully sent',
  ErrorMailSend: 'Sending the Email encountered an error',
  FormFilledIncorrect: 'The form was filled incorrectly',
  UserCreated: 'Your user was successfully created',
  DatabaseError: 'The database operation could not be executed',
  UserExists: 'This user already exists, try logging in',
  IDPwdMissing: 'Imperial ID or password not entered',
  LoginSuccess: 'Login was successfull',
  AccessDenied: 'Access denied, check your Imperial ID and password',
  PwdUpdated: 'Your password was successfully updated, Redirecting in 5 seconds',
  IDLinkWrong: 'User does not exist or link is wrong',
  UserNotFound: 'This user does not exist'
};
