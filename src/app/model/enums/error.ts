export enum Error {
  idTooShort = 'ID is too short',
  pwdTooShort = 'Password is too short',
  realNameTooShort = 'Please provide both given and surname or none',
  emailInvalid = 'Please provide a valid E-Mail address',
  dobInvalid = 'Please provide your date of birth or at least year of birth',
  pwdInvalid = 'Please provide a password that matches the given criteria',
  loginInvalid = 'Imperial ID or password not entered',
  registrationInvalid = 'Form was not filled correctly',
  serverNotReachable = 'Server not reachable, try again',
  validationParamsMissing = 'Your link is not correct, try copying it again. If the problem persists, contact support'
}
