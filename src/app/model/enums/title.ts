export enum Title {
  auth = 'Empire Network',
  legalNotice = 'Legal Notice',
  dataProtection = 'Data Protection Policy',
  copyright = 'Copyright',
  reset = 'Reset Password',
  validate = 'Validate Email'
}
