import { Pipe, PipeTransform } from '@angular/core';
import {serverMsg} from './enums/serverMsg';

@Pipe({
  name: 'translateServer'
})
export class TranslateServerPipe implements PipeTransform {

  transform(value: string): string {
    return serverMsg[value] ? serverMsg[value] : value;
  }

}
