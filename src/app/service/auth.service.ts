import { Injectable } from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static TOKEN = 'EmpireToken';

  constructor(public jwtHelper: JwtHelperService) {}

  public isAuthenticated(): boolean {
    const token = sessionStorage.getItem(AuthService.TOKEN);
    return !this.jwtHelper.isTokenExpired(token);
  }

  public getID(): string {
    if (this.isAuthenticated()) {
      return this.jwtHelper.decodeToken(sessionStorage.getItem(AuthService.TOKEN)).name;
    }
  }

  public getEmail(): string {
    if (this.isAuthenticated()) {
      return this.jwtHelper.decodeToken(sessionStorage.getItem(AuthService.TOKEN)).email;
    }
  }

  public isVerified(): boolean {
    if (this.isAuthenticated()) {
      return this.jwtHelper.decodeToken(sessionStorage.getItem(AuthService.TOKEN)).verified;
    }
  }

  public storeToken(token: string): void {
    sessionStorage.setItem(AuthService.TOKEN, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(AuthService.TOKEN);
  }

  public deleteToken(): void {
    sessionStorage.removeItem(AuthService.TOKEN);
  }
}
