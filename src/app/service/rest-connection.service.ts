import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {ServerMsg, ServerMsgWithToken} from '../model/serverMsg';
import {UserInfo, UserInfoWithToken} from '../model/userInfo';
import {Exists} from '../model/exists';
import {AuthData} from '../model/AuthData';
import {AuthService} from './auth.service';
import {Error} from '../model/enums/error';
import {LanguageService} from './language.service';

@Injectable({
  providedIn: 'root'
})
export class RestConnectionService {
  baseUrl = 'https://rest.cloud-in.space/empire/';
  loginUrl = this.baseUrl + 'authentication';
  registerUrl = this.loginUrl + '/register';
  queryUrl = this.baseUrl + 'user/';
  existsUrl = this.queryUrl + 'exists/';
  emailUrl = this.baseUrl + 'mail';
  validateEmailUrl = this.emailUrl + '/validate';
  updateEmailUrl = this.emailUrl + '/update';
  requestPwdUrl = this.emailUrl + '/reset/';
  resetPwdUrl = this.loginUrl + '/resetPwd';

  constructor(public http: HttpClient, private authService: AuthService, private language: LanguageService) {
  }

  sendRegistration(data: AuthData): Observable<ServerMsg> {
    return this.http.post<ServerMsgWithToken>(this.registerUrl, JSON.stringify(data), this.createOptions())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.msg;
        }
      }), catchError(this.handleError));
  }

  performLogin(data: AuthData): Observable<ServerMsg> {
    return this.http.post<ServerMsgWithToken>(this.loginUrl, JSON.stringify(data), this.createOptions())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.msg;
        }
      }), catchError(this.handleError));
  }

  queryUser(user: string): Observable<UserInfo> {
    return this.http.get<UserInfoWithToken>(this.queryUrl + user, this.createOptionsWithAuth())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.userInfo;
        }
      }), catchError(this.handleError));
  }

  queryHome(): Observable<UserInfo> {
    return this.http.get<UserInfoWithToken>(this.queryUrl, this.createOptionsWithAuth())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.userInfo;
        }
      }), catchError(this.handleError));
  }

  userExists(user: string): Observable<Exists> {
    return this.http.get<Exists>(this.existsUrl + user, this.createOptionsWithAuth())
      .pipe(catchError(this.handleError));
  }

  validateEmail(email: string, id: string): Observable<ServerMsg> {
    return this.http.put<ServerMsgWithToken>(this.emailUrl, JSON.stringify(
      {'email': email, 'id': id, 'lang': this.language.getLanguage()}), this.createOptionsWithAuth())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.msg;
        }
      }), catchError(this.handleError));
  }

  validationEmail(hash: string, id: string): Observable<ServerMsg> {
    hash = hash.replace(new RegExp(' ', 'g'), '+');
    return this.http.put<ServerMsg>(this.validateEmailUrl, JSON.stringify({'hash': hash, 'id': id}), this.createOptions())
      .pipe(catchError(this.handleError));
  }

  requestPwd(id: string): Observable<ServerMsg> {
    return this.http.put<ServerMsg>(this.requestPwdUrl + this.language.getLanguage() + '/' + id, null, this.createOptions())
      .pipe(catchError(this.handleError));
  }

  resetPwd(newPwd: string, hash: string, id: string): Observable<ServerMsg> {
    hash = hash.replace(new RegExp(' ', 'g'), '+');
    return this.http.post<ServerMsgWithToken>(this.resetPwdUrl, JSON.stringify({'newPwd': newPwd, 'id': id, 'hash': hash}),
      this.createOptions())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.msg;
        }
      }), catchError(this.handleError));
  }

  updateEmail(id: string, email: string): Observable<ServerMsg> {
    return this.http.post<ServerMsgWithToken>(this.updateEmailUrl, JSON.stringify({'email': email, 'id': id}), this.createOptionsWithAuth())
      .pipe(map(msg => {
        if (msg && msg.token) {
          this.authService.storeToken(msg.token);
          return msg.msg;
        }
      }), catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error.msg !== undefined) {
      return throwError(error.error.msg);
    } else {
      return throwError(Error.serverNotReachable);
    }
  }

  private createOptions() {
    return {headers: new HttpHeaders({'Content-Type': 'application/json'})};
  }

  private createOptionsWithAuth() {
    return {headers: new HttpHeaders({'Authorization': 'Bearer ' + this.authService.getToken(), 'Content-Type': 'application/json'})};
  }
}
