import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestConnectionService} from '../service/rest-connection.service';
import {Error} from '../model/enums/error';
import {NgbTooltip} from '@ng-bootstrap/ng-bootstrap';
import {NgModel} from '@angular/forms';
import {Regex} from '../model/regex';
import {LanguageService} from '../service/language.service';
import {Title} from '../model/enums/title';

@Component({
  selector: 'empire-reset-link',
  templateUrl: './reset-link.component.html',
  styleUrls: ['./reset-link.component.scss']
})
export class ResetLinkComponent implements OnInit {
  hash: string;
  id: string;
  newPwd: string;
  newMsg: boolean;
  error: boolean;
  alertMessage: string;
  sending: boolean;
  Errors = Error;
  Regex = Regex;

  constructor(private route: ActivatedRoute, private rest: RestConnectionService,
              private router: Router, private language: LanguageService) {
    this.language.setTitle(Title.reset);
  }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe(params => {
        if (params.has('hash') && params.has('id')) {
          this.hash = params.get('hash');
          this.id = params.get('id');
        } else {
          this.alertMessage = Error.validationParamsMissing;
          this.error = true;
          this.newMsg = true;
        }
      });
  }

  public resetPassword(): void {
    this.sending = true;
    this.rest.resetPwd(this.newPwd, this.hash, this.id).subscribe(msg => {
      this.sending = false;
      this.error = msg.msg !== 'PwdUpdated';
      this.newMsg = true;
      this.alertMessage = msg.msg;
      if (msg.msg === 'PwdUpdated') {
        setTimeout(() => this.router.navigate(['/home']), 5000);
      }
    }, error => {
      this.sending = false;
      this.error = true;
      this.newMsg = true;
      this.alertMessage = error;
    });
  }

  processChange(errorTip: NgbTooltip, pwd: NgModel): void {
    if (pwd.valid) {
      errorTip.close();
    } else {
      errorTip.open();
    }
  }

}
