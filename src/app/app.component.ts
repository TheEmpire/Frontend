import { Component } from '@angular/core';

@Component({
  selector: 'empire-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() { }
}
