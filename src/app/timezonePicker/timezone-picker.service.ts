import { Injectable } from '@angular/core';

import * as moment from 'moment-timezone';
import {countryList} from '../model/enums/country';
import {zone} from '../model/enums/zone';

export interface Timezone {
  iso: string;
  zones: string[];
}

@Injectable({
  providedIn: 'root'
})
export class TimezonePickerService {

  /**
   * Convert country ISO code to country name (in english)
   */
  iso2country(iso: string): string {
    return countryList[iso] ? countryList[iso] : iso;
  }

  offsetOfTimezone(zone: string): string {
    let offset = moment.tz(zone).utcOffset();
    const neg = offset < 0;
    if (neg) {
      offset = -1 * offset;
    }
    const hours = Math.floor(offset / 60);
    const minutes = (offset / 60 - hours) * 60;
    return `UTC${neg ? '-' : '+'}${this.rjust(
        hours.toString(),
        2
    )}:${this.rjust(minutes.toString(), 2)}`;
  }

  rjust(string: string, width: number, padding = '0'): string {
    padding = padding || ' ';
    padding = padding.substr(0, 1);
    if (string.length < width) {
      return padding.repeat(width - string.length) + string;
    } else {
      return string;
    }
  }

  /**
   * Gets the list of ISO-codes for all countries
   */
  getCountries(): string[] {
    const res: string[] = [];
    for (const prop of Object.keys(countryList)) {
      res.push(prop);
    }
    return res;
  }

  /**
   * Get the timezones for each country
   */
  getZones(): Timezone[] {
    const res = [];
    for (const prop of Object.keys(zone)) {
      res.push({
        iso: prop,
        zones: zone[prop]
      });
    }
    return res;
  }
}
